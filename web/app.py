from flask import Flask, render_template, Response, abort

app = Flask(__name__)

@app.route("/name.html")
def app_transmit():
    return Response(render_template('name.html'), status=200, mimetype='text/html')#Returns 200 response and send name.html file to client


@app.route("/<flask_url>")
def app_transmit_check(flask_url):
    if flask_url.find("..") != -1 or flask_url.find("//") != -1 or flask_url.find("~") != -1:
        abort(403)
    else:
        abort(404)


@app.errorhandler(404)
def app_notfound(e):
    return render_template('status.html', temp_status = '404'), 404#Returns 404 error and pulls html page with 404 response


@app.errorhandler(403)
def app_invalid(e):
    return render_template('status.html', temp_status = '403'), 403#returns 403 error and pulls html page with 403 response


@app.route("/")
def hello():
    return "UOCIS docker demo! Add /name.html to pull name file. // .. ~ should return error handler 403"

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
