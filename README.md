# About

UO CIS 322 Project 2
Author: Isaac Perks : iperks@uoregon.edu

# Description

A flask/docker/python project to implement basic http status responses. I've added a name.html
file that also responds with a 200 OK header.  I've also added a "status.html" file that takes in 
a error handeler variable and provides a proper header response and status error message.
